
# Assumptions

1. All the customer names are unique
2. All the tickets are unique

# Installation
- `pipenv` can be used to install the development dependencies and enter a shell:

    ```
    pip install pipenv
    pipenv install
    pipenv shell
    ```
- for development:
    ```
    pip install pipenv
    pipenv install --dev
    pipenv shell
    ```
    Note: Ensure flake8 tests are passing by running:
    ```
    flake8 --verbose .
    ```

- starting the server:
  ```
  cd TheaterAPI
  python manage.py runserver
  ```

- `MAX_OCCUPANCY` can be modified in `TheaterAPI/TheaterAPI/settings.py`
- run unittests:
  ```
  cd TheaterAPI
  python manage.py test
  ```

# Sequence Diagram

![Sequence Diagram](./out/seq/Theater%20Seat%20Allotment%20API.svg)


# URL Endpoints
  1.  'seat/occupy/<str:name>/<uuid:ticket>/'
       - Expects a unique string name and UUID ticket
       - Returns a json with seat allotted
       - Seat is allocated randomly from available empty seats     
       - If customer name isn't unique or ticket already exists,
         json response "Customer Already Present" is returned
       - If all the seats are occupied,
      json response "HouseFul" is returned
       - [Test URL](http://localhost:8000/seat/occupy/tester/21e910c4-6bb8-11eb-88e3-9822ef6894e1/)
  2. '/seat/get_info/'
       - expects any one of "ticket", "seat_no", "name",
       - returns all the details in json format
       - If multiple values are provided, priority order is "ticket", "seat_no", "name",
         where "ticket" has highest priority
       - If ticket number provided isn't a UUID, or seat number isn't a number
         json response "Provide Valid Details" is returned
       - If details provided aren't present in the records,
         json response "Provide Valid Details" is returned
       - [Test URL using Name](http://localhost:8000/seat/get_info/?name=tester)
       - [Test URL using Ticket](http://localhost:8000/seat/get_info/?ticket=21e910c4-6bb8-11eb-88e3-9822ef6894e1)
       - [Test URL using seat](http://localhost:8000/seat/get_info/?seat_no=1)
  3. 'seat/vacate/<int:seat_no>'
       - Expects int seat number
       - Returns a json "Seat Vacated" if seat vacated successfully
       - If seat number is out of range or already vacant,
           a json with "Seat Already Vacant" message is returned
       - [Test URL](http://localhost:8000/seat/vacate/1)


# Documentation
## 1. class BiDict
    - Due to Restriction of not using any database, bidict is used to maintain records
    - Inherits inbuilt dict, provides a Bi Directional Dictionary to maintain two way mapping
    - `BiDict.inverse` stores the inverse mapping

## 2. class SeatMap
    - Maintains Customer Name <-> Seat Allotted Mapping


## 3. class TicketMap
    - Maintains Customer Name <-> Ticket Number Mapping

## 4. class Theater
    - instantiates SeatMap and TicketMap to maintain One to One Relationship
        SeatMap: Seat Number <-> Customer Name
        TicketMap:               Customer Name <-> Ticket Number
    - URL endpoints call the public methods
      - `occupy_seat`
      - `vacate_seat`
      - `get_info`
    - Static string variables are used for returning messages

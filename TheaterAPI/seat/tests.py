import json

from django.test import Client
from django.test import TestCase

from .TheaterModel import Theater


class TheaterTestCase(TestCase):
    """
    Test Theater Class
    """

    def test_occupy_seat(self):
        # occupy a seat
        theater = Theater(4)
        response = theater.occupy_seat("person1", "ticket-1")
        self.assertCountEqual(response, {Theater.seat_no: 1})

        # houseful
        theater.occupy_seat("person2", "ticket-2")
        theater.occupy_seat("person3", "ticket-3")
        theater.occupy_seat("person4", "ticket-4")
        response = theater.occupy_seat("person5", "ticket-5")
        self.assertCountEqual(response, {Theater.msg: Theater.houseful})

        theater.vacate_seat(4)
        # duplicate person
        response = theater.occupy_seat("person1", "ticket-5")
        self.assertCountEqual(response, {Theater.msg: Theater.seat_occupied})

        # duplicate ticket
        response = theater.occupy_seat("person5", "ticket-1")
        self.assertCountEqual(response, {Theater.msg: Theater.seat_occupied})

    def test_vacate_seat(self):
        theater = Theater(4)
        seat_no = theater.occupy_seat("person1", "ticket-1")[Theater.seat_no]
        response = theater.vacate_seat(seat_no)
        self.assertCountEqual(response, {Theater.msg: Theater.seat_vacated})

        # vacate empty seat
        empty_seat = 3
        response = theater.vacate_seat(empty_seat)
        self.assertCountEqual(response, {
            Theater.msg: Theater.seat_already_vacant,
        })

        # vacate invalid seat
        invalid_seat = 10
        response = theater.vacate_seat(invalid_seat)
        self.assertCountEqual(response, {
            Theater.msg: Theater.seat_already_vacant,
        })

    def test_get_info(self):
        theater = Theater(4)
        name = "person1"
        ticket = "ticket-1"
        seat_no = theater.occupy_seat(name, ticket)[Theater.seat_no]

        invalid_name = "random-person"
        invalid_ticket = "random-ticket"
        invalid_seat = 999

        # valid request data
        response = theater.get_info(name=name)
        self.assertCountEqual(response, {
            Theater.name: name,
            Theater.ticket: ticket,
            Theater.seat_no: seat_no,
        })

        response = theater.get_info(ticket=ticket)
        self.assertCountEqual(response, {
            Theater.name: name,
            Theater.ticket: ticket,
            Theater.seat_no: seat_no,
        })

        response = theater.get_info(seat_no=seat_no)
        self.assertCountEqual(response, {
            Theater.name: name,
            Theater.ticket: ticket,
            Theater.seat_no: seat_no,
        })

        # invalid request data
        response = theater.get_info(name=invalid_name)
        self.assertCountEqual(response, {Theater.msg: Theater.valid_details})

        response = theater.get_info(ticket=invalid_ticket)
        self.assertCountEqual(response, {Theater.msg: Theater.valid_details})

        response = theater.get_info(seat_no=invalid_seat)
        self.assertCountEqual(response, {Theater.msg: Theater.valid_details})


class SeatTestCase(TestCase):
    """
    Test the seat endpoints.
    The edge cases are already tested in TheaterTestCase
    """

    # http://localhost:8000/seat/occupy/tester/21e910c4-6bb8-11eb-88e3-9822ef6894e1/
    def test_occupy_seat(self):
        c = Client()
        # occupy a seat
        seat_url = '/seat/occupy/person1/21e910c4-6bb8-11eb-88e3-9822ef6894e1/'
        response = c.get(seat_url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {Theater.seat_no: 2},
        )

    # http://localhost:8000/seat/vacate/1
    def test_vacate_seat(self):
        c = Client()
        seat_url = '/seat/occupy/person2/a1b1a4a8-1dbd-4363-a5fd-d76424dc8466/'
        response = c.get('%s' % seat_url)
        response = json.loads(response.content.decode('utf-8'))
        seat_no = response[Theater.seat_no]

        # vacate a seat
        response = c.get(f'/seat/vacate/{seat_no}/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {Theater.msg: Theater.seat_vacated},
        )

    # http://localhost:8000/seat/get_info/?name=tester
    # http://localhost:8000/seat/get_info/?ticket=21e910c4-6bb8-11eb-88e3-9822ef6894e1
    def test_get_info(self):
        c = Client()
        name = "person3"
        ticket = "5da922ba-fca0-4fdf-90d7-ffeb6967d3f2"
        response = c.get(f'/seat/occupy/{name}/{ticket}/')
        response = json.loads(response.content.decode('utf-8'))
        seat_no = response[Theater.seat_no]
        expected_response = {
            Theater.name: name,
            Theater.ticket: ticket,
            Theater.seat_no: seat_no,
        }

        # get_info from customer name
        response = c.get('/seat/get_info/', {'name': name})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            expected_response,
        )

        # get_info from ticket number
        response = c.get('/seat/get_info/', {'ticket': ticket})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            expected_response,
        )

        # get_info from non UUID ticket number
        response = c.get('/seat/get_info/', {'ticket': 'random-text-213'})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {Theater.msg: Theater.valid_details},
        )

        # get_info from seat number
        response = c.get('/seat/get_info/', {'seat_no': seat_no})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            expected_response,
        )
